'use strict'

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    rev = require('gulp-rev'),
    cleanCss = require('gulp-clean-css'),
    flatmap = require('gulp-flatmap'),
    copy = require('gulp-copy'),
    htmlmin = require('gulp-htmlmin');


gulp.task('sass', done => {
    gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
    done();
});

gulp.task('sass:watch', done => {
    gulp.watch('./css/*.scss', ['sass']);
    done();
});

gulp.task('browser-sync', done => {
    var files = ['./*.html', './css/*.css', './img/*.{png, jpg, gif}', './js/*.js']
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    });
    done();
});

gulp.task('default', ['browser-sync'], done => {
    gulp.start('sass:watch');
    done();
});

gulp.task('clean', function(){
    return del(['dist']);
});

gulp.task('copyfonts', function() {
    return gulp
        .src('./node_modules/open-iconic/font/fonts/*')
        .pipe(copy('dist/fonts', {prefix: 4}))
});

gulp.task('imagemin', () =>
    gulp.src('./images/*')
        .pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
        .pipe(gulp.dest('dist/images'))
);

gulp.task('usemin', function(){
    return gulp.src('./*.html')
        .pipe(flatmap(function(stream, file){
            return stream
                .pipe(usemin({
                    css: [rev()],
                    html: [function() { return htmlmin({collapseWhitespace: true})}],
                    js: [uglify(), rev()],
                    inlinejs: [uglify()],
                    inlinecss: [cleanCss(), 'cncat']
                }));
        }))
        .pipe(gulp.dest('dist/'));
});

gulp.task('build', ['clean'], done => {
    gulp.start('copyfonts', 'imagemin', 'usemin');
    done();
});
