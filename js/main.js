$(function(){
    var id = '';
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2000
    });

    $(function() {
        $(document).on('click', 'button[type="button"]', function(event) {
            id = this.id;
            id = '#' + id;
            console.log("Se presionó el Boton con Id :"+ id)
            $(id).removeClass('btn-outline-success');
            $(id).addClass('btn-primary');
            $(id).prop('disabled',true);
        });
    });

    $('#modalLogin').on('show.bs.modal', function(e) {
        console.log('el modal contacto se está mostrando');  
    });
    $('#modalLogin').on('shown.bs.modal', function(e) {
        console.log('el modal contacto se mostró');
    });
    $('#modalLogin').on('hide.bs.modal', function(e) {
        console.log('el modal contacto se está ocultando');
    });
    $('#modalLogin').on('hidden.bs.modal', function(e) {
        console.log('el modal contacto se ocultó');
        $(id).removeClass('btn-primary');
        $(id).addClass('btn-outline-success');
        $(id).prop('disabled',false);
    });
});